import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { MaterialAn } from "./AngularMaterial";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { servicio } from "./Servicios/servicios.component";
import { promocion } from "./Promociones/promociones.component";
import { home } from "./Inicio/inicio.component";
import { ConocenosComponent } from "./Conocenos/conocenos.component";
import { entrar } from "./Login/login.component";
import { NavMatComponent } from "./nav-mat/nav-mat.component";
import { footer } from "./Footer/footer.component";
import { suscribirse } from "./Suscribirse/suscribirse.component";
import { nosotrosexport } from "./Nosotros/nosotros.component";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SuscribcionService } from "./Providers/suscribcion.service";
import { HttpClientModule } from '@angular/common/http';
import { Escuchath } from "./Escucha/escucha.component";

@NgModule({
  declarations: [
    AppComponent,
    NavMatComponent,
    footer,
    suscribirse,
    servicio,
    nosotrosexport,
    promocion,
    home,
    entrar,
    ConocenosComponent,
    Escuchath,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFontAwesomeModule,
    MaterialAn,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    
  ],
  providers: [SuscribcionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
