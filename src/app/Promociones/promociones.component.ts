import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {startWith, map, switchMap} from 'rxjs/operators';

export interface serviciosGroup {
  letter: string;
  names: string[];
}

export const _filter = (opt: string[], value: any): string[] => {
  const filterValue = value.toLowerCase();

  return opt.filter(item => item.toLowerCase().indexOf(filterValue) === 0);
};


@Component({
  selector: 'promociones-themis',
  templateUrl: './promociones.component.html',
  styleUrls: ['./promociones.component.css']
})
export class promocion implements OnInit{
  
serviciosForm: FormGroup = this.fb.group({
    serviciosGroup: '',
  });

  serviciosGroups: serviciosGroup[] = [{
    letter: 'A',
    names: ['No disponible']
  }, {
    letter: 'C',
    names: ['Consorcio']
  }, {
    letter: 'D',
    names: ['Documentos', 'Divocios 2 por 1']
 }, {
    letter: 'H',
    names: ['Herencia Mercantil']
  }, {
    letter: 'M',
    names: ['Matrimonios',]
  }, {
    letter: 'R',
    names: ['Registro Mercantil']
  }, {
    letter: 'S',
    names: ['Servicios']
  }];

  serviciosGroupOptions: Observable<serviciosGroup[]>;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.serviciosGroupOptions = this.serviciosForm.get('serviciosGroup')!.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filterGroup(value))
      );
  }

  private _filterGroup(value: string): serviciosGroup[] {
    if (value) {
      return this.serviciosGroups
        .map(group => ({letter: group.letter, names: _filter(group.names, value)}))
        .filter(group => group.names.length > 0);
    }

    return this.serviciosGroups;
  }
}