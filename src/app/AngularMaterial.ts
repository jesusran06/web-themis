import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import { NgModule } from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import { MatInputModule } from "@angular/material";
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import { MatStepperModule } from "@angular/material";
import { MatDatepickerModule } from "@angular/material";
import { MatSelectModule } from "@angular/material";
import { MatNativeDateModule, MatAutocompleteModule } from "@angular/material";
import { MatTreeModule } from "@angular/material";
import { MatTabsModule } from "@angular/material";
import {MatExpansionModule} from '@angular/material/expansion';
@NgModule({
  imports: [MatButtonModule, MatExpansionModule, MatTabsModule, MatTreeModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule,MatAutocompleteModule, MatCheckboxModule,MatIconModule,MatToolbarModule, MatFormFieldModule, MatDialogModule, MatStepperModule,MatInputModule],
  exports: [MatButtonModule, MatExpansionModule, MatTabsModule,  MatTreeModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule,MatAutocompleteModule, MatCheckboxModule,MatIconModule,MatToolbarModule, MatFormFieldModule, MatDialogModule, MatStepperModule,MatInputModule],
  })
export class MaterialAn { }