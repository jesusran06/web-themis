import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { SuscribcionService } from "../Providers/suscribcion.service";

/**
 * @title Stepper overview
 */
@Component({
  selector: 'pasos-themis',
  templateUrl: 'suscribirse.component.html',
  styleUrls: ['suscribirse.component.css'],
})
export class suscribirse implements OnInit {
  isLinear = false;
  //firstFormGroup: FormGroup;
  //secondFormGroup: FormGroup;
  formSubscribirse: FormGroup;
  opcion = 'Ninguno';
  startDate = new Date(1990, 0, 1);
  constructor(public formBuilder: FormBuilder, public cliente:SuscribcionService) {
    this.loadFormSubscribirse();}

  ngOnInit() {}
    
    
  
  loadFormSubscribirse(){
    this.formSubscribirse= this.formBuilder.group({
      nombre:    ["",[Validators.required,Validators.minLength(2),Validators.maxLength(15)]],
      apellido:  ["",[Validators.required, Validators.minLength(2), Validators.maxLength(15)]],
      correo:    ["",[Validators.required, Validators.email]],
      sexo:      ["",[Validators.required]],
      fecha_nacimiento:    ["",[Validators.required]],
      telefono:   ["",[Validators.required, Validators.minLength(11), Validators.maxLength(11), Validators.pattern(/^[0-9]+(\.?[0-9]+)?$/)]]
    })
  }
  onSubscribe(){
    console.log(this.formSubscribirse.value)
    this.cliente.suscribir(this.formSubscribirse.value)  
  }
}