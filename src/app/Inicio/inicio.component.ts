import { Component, OnInit } from '@angular/core';
import * as M from '../../assets/js/materialize.min.js';
@Component({
  selector: 'inicio-themis',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class home implements OnInit{

  options={indicators:false, interval:3000,height:800};
    
  ngOnInit(){
    var elems = document.querySelectorAll('.slider');
    var instances = M.Slider.init(elems, this.options);
  }
  }


