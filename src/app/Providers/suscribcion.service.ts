import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class SuscribcionService {
  public url: string;
  constructor(public http: HttpClient) {
    this.url = 'https://eos-themis.herokuapp.com/api/';
  }
  suscribir(cliente){
    var headers: any = new HttpHeaders({ 'Content-Type': 'application/json' });
    this.http.post(this.url + '/signup', JSON.stringify(cliente), { headers: headers })
      .subscribe((data: any) => {
        console.log("Suscripcion realizada");
      },
        (error: any) => {
          console.dir(error);
        });
  }
 

}
