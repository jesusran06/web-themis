import { TestBed } from '@angular/core/testing';

import { SuscribcionService } from './suscribcion.service';

describe('SuscribcionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SuscribcionService = TestBed.get(SuscribcionService);
    expect(service).toBeTruthy();
  });
});
